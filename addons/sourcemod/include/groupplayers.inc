#if defined _groupplayers_included
  #endinput
#endif
#define _groupplayers_included

/*********************************************************
 * В группе ли игрок
 *
 * @true on match , false if not
 *********************************************************/
native bool InGroup(int client);

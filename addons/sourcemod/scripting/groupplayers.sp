#pragma semicolon 1

stock void debugMessage(const char[] message, any ...)
{
	char szMessage[256], szPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, szPath, sizeof(szPath), "logs/debug_groupplayers.txt");
	VFormat(szMessage, sizeof(szMessage), message, 2);
	LogToFile(szPath, szMessage);
}
#define dbgMsg(%0) debugMessage(%0)

#define PLUGIN_AUTHOR "boomix & ShaRen"
#define VERSION "1.01.16"

#include <cstrike>			// for CS_SetClientClanTag
#include <SteamWorks>
#include <smlib>
#include <csgo_colors>
#include <jail_stats>

#pragma newdecls required

#define LoopAllPlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1) && !IsFakeClient(%1))

bool b_InGroup[MAXPLAYERS + 1];

public Plugin myinfo = 
{
	name		= "Group players",
	author		= PLUGIN_AUTHOR,
	description	= "Gives more credits for players that are in steam group",
	version		= VERSION,
	url			= "Servers-Info.Ru"
}

public void OnPluginStart()
{
	for(int i=1; i<MAXPLAYERS; i++)
		if(IsPlayerValid(i)) {
			if (SteamWorks_HasLicenseForApp(i, 730) == k_EUserHasLicenseResultHasLicense) {
				dbgMsg("OnPluginStart() SteamWorks_GetUserGroupStatus for %i", i);
				SteamWorks_GetUserGroupStatus(i, 103582791431452545);
			}
		}
	CreateTimer(135.0, Message, _, TIMER_REPEAT);
	RegAdminCmd("sm_testgroup", GetMembers, ADMFLAG_ROOT);
	SteamWorks_SetGameDescription("Servers-Info JAIL");
}

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	CreateNative("InGroup", Native_InGroup);
	RegPluginLibrary("groupplayers");
	return APLRes_Success;
}

public int Native_InGroup(Handle plugin, int numParams)
{
	int iClient = GetNativeCell(1);
	
	if(!IsClientInGame(iClient) && !IsClientConnected(iClient))
		ThrowNativeError(SP_ERROR_INDEX, "Client index %i is invalid", iClient);
	
	return (b_InGroup[iClient])? true:false;
}

public Action GetMembers(int client, int args)
{
	int inG, G, noS;
	bool bNoG[MAXPLAYERS], bNoSteam[MAXPLAYERS];
	for(int i=1; i<MAXPLAYERS; i++)
		if(IsPlayerValid(i)) {
			if (SteamWorks_HasLicenseForApp(i, 730) == k_EUserHasLicenseResultHasLicense) {
				dbgMsg("GetMembers() SteamWorks_GetUserGroupStatus for %i", i);
				SteamWorks_GetUserGroupStatus(i, 103582791431452545);
				if(b_InGroup[i]) {
					PrintToServer("[S] inGr %N", i);
					inG++;
				} else bNoG[i] = true;
				G++;
			} else {
				bNoSteam[i] = true;
				noS++;
			}
		}
	for(int i=1; i<MAXPLAYERS; i++)
		if(bNoG[i])
			PrintToServer("[S]      %N", i);
	for(int i=1; i<MAXPLAYERS; i++)
		if(bNoSteam[i])
			PrintToServer("[P]      %N", i);
	PrintToServer("В группе %i/%i, остальные %i No-Steam", inG, G, noS);
}

public Action Message(Handle timer)
{
	for (int i=1; i<GetMaxClients(); i++)
		if (IsPlayerValid(i)) {
			if (IsPlayerSteam(i)) {
				if (!b_InGroup[i]) {
					CGOPrintToChat(i, "{RED}[S-IC Group] Вы не состоите в нашей группе в Steam!!!");
					CGOPrintToChat(i, "{RED}[S-IC Group] {GREEN}Все участники группы Servers-Info получают на {RED}20%% больше кредитов");
					CGOPrintToChat(i, "{RED}[S-IC Group] {GREEN}Просто вступите в нашу группу в стиме {BLUE}Servers-Info {GREEN}и получайте дополнительные кредиты");
					CGOPrintToChat(i, "{RED}[S-IC Group] {GREEN}Чтобы всупить напиши {BLUE}group{GREEN} или {BLUE}join{GREEN} и жми {BLUE}Присоединиться");
				} else CGOPrintToChat(i, "{BLUE}[S-IC Group] {GREEN}Спасибо что состоите в нашей группе в STEAM, за это вам будут начисляться дополнительные кредиты");
			} else if (GetRank (i, 2) > 5000 || GetRank (i, 3) > 10000) {
				CGOPrintToChat(i, "{BLUE}[S-IC] {GREEN}Если вы решили перейти на стим-версию, но не хотите терять свои очки, кредиты и статистику,");
				CGOPrintToChat(i, "{BLUE}[S-IC] {GREEN}то мы можем перенести их после вашего перехода на стим версию");
				CGOPrintToChat(i, "{BLUE}[S-IC] {GREEN}напишите об этом тех. или гл. админу");
			}
		}
	return Plugin_Continue;
}

public Action Shop_OnCreditsGiven(int iClient, int &iCredits, int by_who)
{
	if (IsPlayerValid(iClient) && IsPlayerSteam(iClient)) {
		float fMultiplier = 1.2;
		if (b_InGroup[iClient]) {
			if(fMultiplier && iCredits != 0 && by_who == -1) {
				iCredits = RoundToCeil(float(iCredits)*fMultiplier);
				CGOPrintToChat(iClient, "{BLUE}[S-IC Group] {GREEN}Вам дополнительно начисленно {DEFAULT}%i {GREEN}кр. т.к. вы состоие в нашей STEAM группе", RoundToCeil(float(iCredits)*(fMultiplier-1)) );
			}
		} else {
			CGOPrintToChat(iClient, "{RED}[S-IC Group] {GREEN}Вы получили БЫ {DEFAULT}%i {GREEN}кр. если бы состояли в нашей STEAM группе", RoundToCeil(float(iCredits)*(fMultiplier-1)) );
			CGOPrintToChat(iClient, "{RED}[S-IC Group] {BLUE}https://steamcommunity.com/groups/Servers-Info");
		}
	}
	return Plugin_Continue;
}

public void OnClientPutInServer(int iClient)
{
	b_InGroup[iClient] = false;
	if (SteamWorks_HasLicenseForApp(iClient, 730) == k_EUserHasLicenseResultHasLicense) {
		dbgMsg("OnClientPutInServer(%i) SteamWorks_GetUserGroupStatus", iClient);
		SteamWorks_GetUserGroupStatus(iClient, 103582791431452545);
	}
}

public int SteamWorks_OnClientGroupStatus(int authid, int groupid, bool bIsMember, bool isOfficer)
{
	int iClient = GetClientFromAuthID(authid);
	if (iClient > 0)
		b_InGroup[iClient] = (bIsMember)? true:false;
}

int GetClientFromAuthID(int authid)
{
	LoopAllPlayers(i) {
		char sAuthstring[50];
		GetClientAuthId(i, AuthId_Steam3, sAuthstring, sizeof(sAuthstring));
		char sAuthstring2[50];
		IntToString(authid, sAuthstring2, sizeof(sAuthstring2));
		if(StrContains(sAuthstring, sAuthstring2) != -1)
			return i;
	}
	LogError("Unable GetClientFromAuthID authid = %i", authid);
	return -1;
}

bool IsPlayerValid(int iClient)
{
	return (!IsClientConnected(iClient) || !IsClientInGame(iClient) || IsFakeClient(iClient))? false:true;
}

bool IsPlayerSteam(int iClient)
{
	return (SteamWorks_HasLicenseForApp(iClient, 730) != k_EUserHasLicenseResultHasLicense)? false:true;
}

